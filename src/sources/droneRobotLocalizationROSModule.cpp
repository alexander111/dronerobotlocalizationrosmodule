//////////////////////////////////////////////////////
//  droneExamplePackageROSModuleSource.cpp
//
//  Created on: 29 Feb, 2016
//      Author: Hriday
//
//////////////////////////////////////////////////////


#include "droneRobotLocalizationROSModule.h"


// using namespaces only in cpp files!!
using namespace std;



droneRobotLocalizationROSModule::droneRobotLocalizationROSModule() : DroneModule(droneModule::active)
{

    return;
}


droneRobotLocalizationROSModule::~droneRobotLocalizationROSModule()
{
	close();
	return;
}

void droneRobotLocalizationROSModule::readParameters()
{
    //Config file
    ros::param::get("~config_file", configFile);
    if ( configFile.length() == 0)
    {
        configFile="robot_localization.xml";
    }
}

bool droneRobotLocalizationROSModule::init(std::string configFile)
{
    readConfigs(configFile);
    DroneModule::init();

    return true;
}

bool droneRobotLocalizationROSModule::readConfigs(std::string configFile)
{

    try
    {
        XMLFileReader my_xml_reader(configFile);

        init_position_x   = my_xml_reader.readDoubleValue("take_off_site:position:x");
        init_position_y   = my_xml_reader.readDoubleValue("take_off_site:position:y");
        init_position_z   = my_xml_reader.readDoubleValue("take_off_site:position:z");

        init_yaw          = my_xml_reader.readDoubleValue("take_off_site:attitude:yaw");
        init_roll         = my_xml_reader.readDoubleValue("take_off_site:attitude:roll");
        init_pitch        = my_xml_reader.readDoubleValue("take_off_site:attitude:pitch");

        //Converting degrees to radians
        init_yaw    = init_yaw*(M_PI/180.0);
        init_roll   = init_roll*(M_PI/180.0);
        init_pitch  = init_pitch*(M_PI/180.0);

        altitude_co_z     = my_xml_reader.readDoubleValue("covariances:altitude:z");
        altitude_co_dz    = my_xml_reader.readDoubleValue("covariances:altitude:dz");


        optical_flow_co_x = my_xml_reader.readDoubleValue("covariances:optical_flow:dx");
        optical_flow_co_y = my_xml_reader.readDoubleValue("covariances:optical_flow:dy");

        pitch_cmd_co      = my_xml_reader.readDoubleValue("covariances:controller_odom:pitch_command");
        roll_cmd_co       = my_xml_reader.readDoubleValue("covariances:controller_odom:roll_command");
        daltitude_cmd_co  = my_xml_reader.readDoubleValue("covariances:controller_odom:daltitude_command");
        dyaw_cmd_co       = my_xml_reader.readDoubleValue("covariances:controller_odom:dyaw_command");
    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}


void droneRobotLocalizationROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    DroneModule::open(nIn);

    //read the parameters from the launch file
    readParameters();
    //Init
    if(!init(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/"+configFile))
    {
        cout<<"Error init"<<endl;
        return;
    }



    //// TOPICS
    //Subscribers
    droneImuSub              = n.subscribe("imu", 1, &droneRobotLocalizationROSModule::droneImuCallback, this);
    droneCommandPitchRollSub = n.subscribe("command/pitch_roll",1,&droneRobotLocalizationROSModule::droneCommandPitchRollCallback, this);
    droneCommandDaltitudeSub = n.subscribe("command/dAltitude",1,&droneRobotLocalizationROSModule::droneCommandDaltitudeCallback, this);
    droneCommandDyawSub      = n.subscribe("command/dYaw",1, &droneRobotLocalizationROSModule::droneCommandDyawCallback, this);
    droneAltitudeSub         = n.subscribe("altitude",1,&droneRobotLocalizationROSModule::droneAltitudeCallback, this);
    droneSpeedsSub           = n.subscribe("ground_speed",1,&droneRobotLocalizationROSModule::droneSpeedsCallback, this);
    droneOdomFilteredSub     = n.subscribe("odometry/filtered",1,&droneRobotLocalizationROSModule::droneOdometryFilteredCallback, this);
    droneRotationAnglesSub   = n.subscribe("rotation_angles",1,&droneRobotLocalizationROSModule::droneRotationAnglesCallback, this);
    dronePnPPoseSub          = n.subscribe("vb_estimated_pose/rpnp_pose",1,&droneRobotLocalizationROSModule::dronePnPPoseCallback, this);
    droneGPSDataSub          = n.subscribe("mavros/global_position/raw/fix",1,&droneRobotLocalizationROSModule::droneGPSDataCallback, this);
    droneHectorSlamdPoseSub  = n.subscribe("/poseupdate",1,&droneRobotLocalizationROSModule::droneHectorSlamPoseCallback, this);

    //Publishers
    droneImuPub             = n.advertise<sensor_msgs::Imu>("imu/data", 1, true);
    droneRotationAnglesPub  = n.advertise<nav_msgs::Odometry>("rotation_angles/data",1,true);
    droneOdomPub            = n.advertise<nav_msgs::Odometry>("controller/odom/data",1,true);
    droneAltitudePub        = n.advertise<nav_msgs::Odometry>("altitude/data",1,true);
    droneGroundSpeedsPub    = n.advertise<nav_msgs::Odometry>("optical_flow/data",1,true);
    dronePnPPosePub         = n.advertise<nav_msgs::Odometry>("PnP_pose/data",1, true);
    droneGPSdataPub         = n.advertise<sensor_msgs::NavSatFix>("gps/fix_data",1, true);
    droneHectorSlamDataPub  = n.advertise<nav_msgs::Odometry>("hector_slam/data",1,true);
    droneSetModePub         = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("set_pose",1,true);
    droneEstimatedPosePub   = n.advertise<droneMsgsROS::dronePose>("EstimatedPose_droneGMR_wrt_GFF",1,true);
    droneEstimatedSpeedsPub = n.advertise<droneMsgsROS::droneSpeeds>("EstimatedSpeed_droneGMR_wrt_GFF",1,true);

    //Flag of module opened
    droneModuleOpened=true;

    //autostart module!
//    moduleStarted=true;

	
	//End
	return;
}


void droneRobotLocalizationROSModule::close()
{
    DroneModule::close();

    //Do stuff

    return;
}


bool droneRobotLocalizationROSModule::resetValues()
{
    //Reseting the ekf

    ResetPose.header.stamp = ros::Time::now();
    ResetPose.header.frame_id = "speeds_odom";
    ResetPose.pose.pose.position.x    = init_position_x;
    ResetPose.pose.pose.position.y    = init_position_y;
    ResetPose.pose.pose.position.z    = init_position_z;

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(init_roll,init_pitch,init_yaw);

    ResetPose.pose.pose.orientation.x = quaternion.getX();
    ResetPose.pose.pose.orientation.y = quaternion.getY();
    ResetPose.pose.pose.orientation.z = quaternion.getZ();
    ResetPose.pose.pose.orientation.w = quaternion.getW();

    for (size_t ind = 0; ind < 36; ind+=7)
    {
      ResetPose.pose.covariance[ind] = 1e-6;
    }

    droneSetModePub.publish(ResetPose);
    return true;
}


bool droneRobotLocalizationROSModule::startVal()
{
    //Reseting the ekf

    ResetPose.header.stamp = ros::Time::now();
    ResetPose.header.frame_id = "speeds_odom";
    ResetPose.pose.pose.position.x    = init_position_x;
    ResetPose.pose.pose.position.y    = init_position_y;
    ResetPose.pose.pose.position.z    = init_position_z;

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(init_roll,init_pitch,init_yaw);

    ResetPose.pose.pose.orientation.x = quaternion.getX();
    ResetPose.pose.pose.orientation.y = quaternion.getY();
    ResetPose.pose.pose.orientation.z = quaternion.getZ();
    ResetPose.pose.pose.orientation.w = quaternion.getW();

    for (size_t ind = 0; ind < 36; ind+=7)
    {
      ResetPose.pose.covariance[ind] = 1e-6;
    }

    droneSetModePub.publish(ResetPose);

    //End
    return DroneModule::startVal();
}


bool droneRobotLocalizationROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool droneRobotLocalizationROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    return true;
}

void droneRobotLocalizationROSModule::droneImuCallback(const sensor_msgs::Imu &msg)
{
    //Publishing IMU data to the EKF
    Imu_data.header.stamp = msg.header.stamp;
    Imu_data.header.frame_id = "fcu";

    Imu_data.orientation            = msg.orientation;
    Imu_data.orientation_covariance = msg.orientation_covariance;
//    Imu_data.orientation_covariance[0] = 1.0;
//    Imu_data.orientation_covariance[4] = 1.0;
//    Imu_data.orientation_covariance[8]=  1.0;

    //converting from NED frame to ENU frame for the robot_localization
    Imu_data.angular_velocity.x = +1*msg.angular_velocity.x;
    Imu_data.angular_velocity.y = -1*msg.angular_velocity.y;
    Imu_data.angular_velocity.z = -1*msg.angular_velocity.z;
    Imu_data.angular_velocity_covariance = msg.angular_velocity_covariance;
//    Imu_data.angular_velocity_covariance[0] = 100000;
//    Imu_data.angular_velocity_covariance[4] = 100000;
//    Imu_data.angular_velocity_covariance[8] = 100000;

    //converting from NED frame to ENU frame for the robot_localization
    Imu_data.linear_acceleration.x = +1*msg.linear_acceleration.x;
    Imu_data.linear_acceleration.y = -1*msg.linear_acceleration.y;
    Imu_data.linear_acceleration.z = -1*msg.linear_acceleration.z;
    Imu_data.linear_acceleration_covariance = msg.linear_acceleration_covariance;
//    Imu_data.linear_acceleration_covariance[0] = 100000;
//    Imu_data.linear_acceleration_covariance[4] = 100000;
//    Imu_data.linear_acceleration_covariance[8] = 100000;


    PublishImuData();
    return;
}

//This data is Published only if the drone does not publish IMU data
void droneRobotLocalizationROSModule::droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped &msg)
{
    rotationAngleData.header.stamp    = ros::Time::now();
    rotationAngleData.header.frame_id = "speeds_odom";
    rotationAngleData.child_frame_id  = "fcu";

    //convert from degrees to radians
    roll_rad      =  (msg.vector.x) * (M_PI/180);
    pitch_rad     = -(msg.vector.y) * (M_PI/180);
    yaw_rad       = -(msg.vector.z) * (M_PI/180);

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll_rad, pitch_rad, yaw_rad);

    rotationAngleData.pose.pose.orientation.x = quaternion.getX();
    rotationAngleData.pose.pose.orientation.y = quaternion.getY();
    rotationAngleData.pose.pose.orientation.z = quaternion.getZ();
    rotationAngleData.pose.pose.orientation.w = quaternion.getW();
    rotationAngleData.pose.covariance[21] = 0.33;
    rotationAngleData.pose.covariance[28] = 0.33;
    rotationAngleData.pose.covariance[35] = 0.33;

    PublishRotationAnglesData();
}

void droneRobotLocalizationROSModule::droneCommandPitchRollCallback(const droneMsgsROS::dronePitchRollCmd &msg)
{
    PitchRollCmd.pitchCmd = msg.pitchCmd;
    PitchRollCmd.rollCmd  = msg.rollCmd;
    receivedPitchRoll = true;
    return;
}

void droneRobotLocalizationROSModule::droneCommandDaltitudeCallback(const droneMsgsROS::droneDAltitudeCmd &msg)
{
    DaltitudeCmd.dAltitudeCmd = msg.dAltitudeCmd;
    receivedDaltitude = true;
    return;
}

void droneRobotLocalizationROSModule::droneCommandDyawCallback(const droneMsgsROS::droneDYawCmd &msg)
{
    DyawCmd.dYawCmd = msg.dYawCmd;
    receivedDyaw = true;

    PublishOdomData();
    return;
}

void droneRobotLocalizationROSModule::droneAltitudeCallback(const droneMsgsROS::droneAltitude &msg)
{
     //Publishing Alitude data to the EKF
     altitudeData.header.stamp    = msg.header.stamp;
     altitudeData.header.frame_id = "speeds_odom";
     altitudeData.child_frame_id  = "fcu";

     altitudeData.pose.pose.position.z = -msg.altitude;
     altitudeData.twist.twist.linear.z = -msg.altitude_speed;
     altitudeData.pose.covariance[14]  = altitude_co_z;
     altitudeData.twist.covariance[14] = altitude_co_dz;


     PublishAltitudeData();
     return;
}

void droneRobotLocalizationROSModule::droneSpeedsCallback(const droneMsgsROS::vector2Stamped &msg)
{
    //Publishing optical flow data to the EKF
    groundSpeedsData.header.stamp = ros::Time::now();
    groundSpeedsData.header.frame_id = "speeds_odom";
    groundSpeedsData.child_frame_id  = "fcu";

    groundSpeedsData.twist.twist.linear.x  =  msg.vector.x;
    groundSpeedsData.twist.twist.linear.y  = -msg.vector.y;
    groundSpeedsData.twist.covariance[0]   = optical_flow_co_x;
    groundSpeedsData.twist.covariance[7]   = optical_flow_co_y;

    PublishSpeedsData();
    return;
}

void droneRobotLocalizationROSModule::dronePnPPoseCallback(const geometry_msgs::Pose &msg)
{
    PnPPoseData.header.stamp = ros::Time::now();
    PnPPoseData.child_frame_id  = "fcu";
    PnPPoseData.header.frame_id = "speeds_odom";

    PnPPoseData.pose.pose.position.x = msg.position.x;
    PnPPoseData.pose.pose.position.y = msg.position.y;
    PnPPoseData.pose.pose.position.z = msg.position.z;

    std::cout << EstimatedPose.x << std::endl;
    if(EstimatedPose.x >= 6.0 && EstimatedPose.x <= 10.0){
      PnPPoseData.pose.covariance[0]  = 1.0;
      PnPPoseData.pose.covariance[7]  = 1.0;
      PnPPoseData.pose.covariance[14] = 1.0;
    }
    else{
      PnPPoseData.pose.covariance[0]  = 10000.0;
      PnPPoseData.pose.covariance[7]  = 10000.0;
      PnPPoseData.pose.covariance[14] = 10000.0;
    }

    PublishPnPData();

}

void droneRobotLocalizationROSModule::droneGPSDataCallback(const sensor_msgs::NavSatFix &msg)
{
    gps_data.header.stamp             = msg.header.stamp;
    gps_data.header.frame_id          = "gps";
    gps_data.latitude                 = msg.latitude;
    gps_data.longitude                = msg.longitude;
    gps_data.altitude                 = msg.altitude;
    gps_data.position_covariance[0]   = 100;
    gps_data.position_covariance[4]   = 100;
    gps_data.position_covariance[8]   = 100;
    gps_data.position_covariance_type = 1;

    PublishGPSData();
}

void droneRobotLocalizationROSModule::droneHectorSlamPoseCallback(const geometry_msgs::PoseWithCovarianceStamped &msg)
{
     HectorSlamPoseData.child_frame_id  = "fcu";
     HectorSlamPoseData.header.frame_id = "speeds_odom";
     HectorSlamPoseData.header.stamp    = msg.header.stamp;

//     geometry_msgs::PoseStamped PoseIn;
//     geometry_msgs::PoseStamped PoseOut;

//     PoseIn.header.stamp    = msg.header.stamp;
//     PoseIn.header.frame_id = "/map";
//     PoseIn.pose            = msg.pose;

//     try{
//     listener.transformPose("/odom",ros::Time(0) ,PoseIn, "/map",PoseOut);
//     std::cout << "PoseIn" <<  PoseIn << std::endl;
//     std::cout << "PoseOut" << PoseOut << std::endl;
//     }
//     catch (tf::TransformException ex)
//     {
//         ROS_ERROR("%s",ex.what());
//         ros::Duration(1.0).sleep();
//     }

//     geometry_msgs::PoseStamped PoseDiff;
//     PoseDiff.pose.position.x = PoseIn.pose.position.x - PoseOut.pose.position.x;
//     PoseDiff.pose.position.y = PoseIn.pose.position.y - PoseOut.pose.position.y;
//     std::cout << "PoseDiff " << PoseDiff << std::endl;

     HectorSlamPoseData.pose.pose.position.x = msg.pose.pose.position.x ;  /*PoseIn.pose.position.x */;
     HectorSlamPoseData.pose.pose.position.y = msg.pose.pose.position.y ;  /*PoseIn.pose.position.y*/;
     HectorSlamPoseData.pose.covariance      = msg.pose.covariance;

     PublishHectorSlamData();

}

void droneRobotLocalizationROSModule::droneOdometryFilteredCallback(const nav_msgs::Odometry &msg)
{
//-----------------------------------------------------------------------------------------------------------
// Estimated pose from the EKF
//-----------------------------------------------------------------------------------------------------------

    EstimatedPose.x = msg.pose.pose.position.x;
    EstimatedPose.y = msg.pose.pose.position.y;
    EstimatedPose.z = msg.pose.pose.position.z;

    //converting the orientation from quaternion to roll, pitch and yaw
    tf::Quaternion q(msg.pose.pose.orientation.x,msg.pose.pose.orientation.y,
                     msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);

    tf::Matrix3x3 m(q);
    m.getEulerYPR(yaw, pitch, roll);

    EstimatedPose.pitch = pitch;
    EstimatedPose.roll  = roll;
    EstimatedPose.yaw   = yaw;

//-------------------------------------------------------------------------------------------------------------
// Estimated Speeds from the EKF
//-------------------------------------------------------------------------------------------------------------

    //transformVector funtion requires the frame_id as well as the current time
    vin.header.stamp = ros::Time::now();
    vin.header.frame_id = "/base_link";
    vin.vector.x = msg.twist.twist.linear.x;
    vin.vector.y = msg.twist.twist.linear.y;
    vin.vector.z = msg.twist.twist.linear.z;

    //Using tf to convert the speeds (twist.linear) messages from base_link frame to odom frame as
    //the Aerostack requires speeds in odom frame (i.e world frame)
    try{
    listener.transformVector("/odom",ros::Time(0) ,vin, "/base_link",vout);
    }
    catch (tf::TransformException ex)
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }

    EstimatedSpeeds.dx   =  vout.vector.x;
    EstimatedSpeeds.dy   =  vout.vector.y;
    EstimatedSpeeds.dz   = -vout.vector.z;
    EstimatedSpeeds.dyaw = -msg.twist.twist.angular.z;

    PublishEstimatedData();
    return;
}


void droneRobotLocalizationROSModule::PublishImuData()
{
    if(moduleStarted == true)
     droneImuPub.publish(Imu_data);
    return;
}

void droneRobotLocalizationROSModule::PublishRotationAnglesData()
{
  if(moduleStarted == true)
    droneRotationAnglesPub.publish(rotationAngleData);
  return;
}

void droneRobotLocalizationROSModule::PublishOdomData()
{
      //Publishing the Controller command data to the ekf
      controllerOdometryData.header.stamp = ros::Time::now();
      controllerOdometryData.header.frame_id = "speeds_odom";
      controllerOdometryData.child_frame_id  = "fcu";

      controllerOdometryData.twist.twist.linear.x  = -PitchRollCmd.pitchCmd;
      controllerOdometryData.twist.twist.linear.y  = -PitchRollCmd.rollCmd;
      controllerOdometryData.twist.twist.linear.z  = DaltitudeCmd.dAltitudeCmd;
      controllerOdometryData.twist.twist.angular.z = +DyawCmd.dYawCmd;
      controllerOdometryData.twist.covariance[0]  = pitch_cmd_co;
      controllerOdometryData.twist.covariance[7]  = roll_cmd_co;
      controllerOdometryData.twist.covariance[14] = daltitude_cmd_co;
      controllerOdometryData.twist.covariance[29] = dyaw_cmd_co;

      if(moduleStarted == true)
        droneOdomPub.publish(controllerOdometryData);
      return;
}

void droneRobotLocalizationROSModule::PublishAltitudeData()
{
     if(moduleStarted == true)
       droneAltitudePub.publish(altitudeData);
     return;
}

void droneRobotLocalizationROSModule::PublishSpeedsData()
{
    if(moduleStarted == true)
      droneGroundSpeedsPub.publish(groundSpeedsData);
    return;
}

void droneRobotLocalizationROSModule::PublishPnPData()
{
    if(moduleStarted == true)
        dronePnPPosePub.publish(PnPPoseData);
    return;

}

void droneRobotLocalizationROSModule::PublishGPSData()
{
    if(moduleStarted == true)
        droneGPSdataPub.publish(gps_data);
      return;

}

void droneRobotLocalizationROSModule::PublishHectorSlamData()
{
  if(moduleStarted == true)
      droneHectorSlamDataPub.publish(HectorSlamPoseData);
    return;
}

void droneRobotLocalizationROSModule::PublishEstimatedData()
{
    if(moduleStarted == true)
    {
      droneEstimatedPosePub.publish(EstimatedPose);
      droneEstimatedSpeedsPub.publish(EstimatedSpeeds);
    }
    return;
}
