//////////////////////////////////////////////////////
//  DroneExamplePackageROSModule.h
//
//  Created on: 29 Feb, 2016
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////

#ifndef DRONE_EXAMPLE_PACKAGE_ROS_MODULE_H
#define DRONE_EXAMPLE_PACKAGE_ROS_MODULE_H




//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>

//String stream
//std::istringstream
#include <sstream>


// ROS
#include "ros/ros.h"



//Drone module
#include "droneModuleROS.h"


//Main class -> Algorithm developed!


//Messages in
//ROS
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <sensor_msgs/NavSatFix.h>
#include "tf/transform_datatypes.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"

//OpenCV if needed
//#include <opencv2/opencv.hpp>


//Messages out
//Keypoints: Messages out
#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include <droneMsgsROS/droneAltitude.h>
#include <droneMsgsROS/vector2Stamped.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneSpeeds.h>


//Ground Robots: Messages out
//#include <droneMsgsROS/targetInImage.h>
//#include <droneMsgsROS/vectorTargetsInImageStamped.h>
#include "xmlfilereader.h"





/////////////////////////////////////////
// Class DroneKeypointsGridDetectorROSModule
//
//   Description
//
/////////////////////////////////////////
class droneRobotLocalizationROSModule : public DroneModule
{	
protected:



    //Config files path names
protected:
    //std::string camCalibParamFile;
    //std::string gridIntersectionsParamFile;


    //Variables
public:
    sensor_msgs::Imu Imu_data;
    nav_msgs::Odometry controllerOdometryData;
    droneMsgsROS::dronePitchRollCmd PitchRollCmd;
    droneMsgsROS::droneDAltitudeCmd DaltitudeCmd;
    droneMsgsROS::droneDYawCmd      DyawCmd;
    nav_msgs::Odometry altitudeData;
    nav_msgs::Odometry groundSpeedsData;
    nav_msgs::Odometry rotationAngleData;
    nav_msgs::Odometry PnPPoseData;
    nav_msgs::Odometry HectorSlamPoseData;
    geometry_msgs::TransformStamped transform;
    geometry_msgs::PoseWithCovarianceStamped ResetPose;
    tf::TransformBroadcaster broadcastTransform;
    droneMsgsROS::vector2Stamped speeds;
    droneMsgsROS::dronePose EstimatedPose;
    droneMsgsROS::droneSpeeds EstimatedSpeeds;
    bool receivedPitchRoll, receivedDaltitude, receivedDyaw;
    double yaw, pitch, roll;
    tf::TransformListener listener;
    geometry_msgs::Vector3Stamped vin, vout;
    sensor_msgs::NavSatFix gps_data;
    tf::StampedTransform hector_transform;

public:
    std::string configFile;
    double init_position_x,init_position_y, init_position_z;
    double init_pitch, init_roll, init_yaw;
    double optical_flow_co_x, optical_flow_co_y;
    double altitude_co_z, altitude_co_dz;
    double pitch_cmd_co, roll_cmd_co, dyaw_cmd_co, daltitude_cmd_co;
    double pitch_rad, roll_rad, yaw_rad;

    //subscribers
protected:

    //Subscriber
    ros::Subscriber droneImuSub;
    ros::Subscriber droneRotationAnglesSub;
    ros::Subscriber droneCommandPitchRollSub;
    ros::Subscriber droneCommandDaltitudeSub;
    ros::Subscriber droneCommandDyawSub;
    ros::Subscriber droneAltitudeSub;
    ros::Subscriber droneSpeedsSub;
    ros::Subscriber droneOdomFilteredSub;
    ros::Subscriber dronePnPPoseSub;
    ros::Subscriber droneGPSDataSub;
    ros::Subscriber droneHectorSlamdPoseSub;
public:
    //Callback function
    void droneImuCallback(const sensor_msgs::Imu& msg);
    void droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
    void droneCommandPitchRollCallback(const droneMsgsROS::dronePitchRollCmd& msg);
    void droneCommandDaltitudeCallback(const droneMsgsROS::droneDAltitudeCmd& msg);
    void droneCommandDyawCallback(const droneMsgsROS::droneDYawCmd& msg);
    void droneAltitudeCallback(const droneMsgsROS::droneAltitude& msg);
    void droneSpeedsCallback(const droneMsgsROS::vector2Stamped& msg);
    void droneOdometryFilteredCallback(const nav_msgs::Odometry& msg);
    void dronePnPPoseCallback(const geometry_msgs::Pose& msg);
    void droneGPSDataCallback(const sensor_msgs::NavSatFix& msg);
    void droneHectorSlamPoseCallback(const geometry_msgs::PoseWithCovarianceStamped& msg);

    //publishers
protected:
     //Publisher
     ros::Publisher droneImuPub;
     ros::Publisher droneRotationAnglesPub;
     ros::Publisher droneOdomPub;
     ros::Publisher droneAltitudePub;
     ros::Publisher droneGroundSpeedsPub;
     ros::Publisher droneSetModePub;
     ros::Publisher droneEstimatedPosePub;
     ros::Publisher droneEstimatedSpeedsPub;
     ros::Publisher dronePnPPosePub;
     ros::Publisher droneGPSdataPub;
     ros::Publisher droneHectorSlamDataPub;

   //Publisher functions
public:
    void PublishOdomData();
    void PublishRotationAnglesData();
    void PublishAltitudeData();
    void PublishSpeedsData();
    void PublishImuData();
    void PublishEstimatedData();
    void PublishPnPData();
    void PublishGPSData();
    void PublishHectorSlamData();

public:
    droneRobotLocalizationROSModule();
    ~droneRobotLocalizationROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
    void close();

public:
    bool init(std::string configFile);
    bool readConfigs(std::string configFile);
    void readParameters();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

#endif
